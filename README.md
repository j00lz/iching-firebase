## iChing Journaling Application v0.0.5

> Create, save and review all your question and answers.

## Easy Wins!

## Roadmap for v0.1.0

* Refactor code especially firebase related calls


### Roadmap for v1.0


* Nice display for reading....

## Required functionality

* Implement journal entries linking to a reading id
* Implement previous readings list
* Tidy up user login details display
* Tidy up all components and views
* Data verification and privacy rules

### Ideas for future versions

* Statistics shows on readings
* Improve all listing components
* Other signin provider
* Profile Homepage
* Image Upload for avatar
* email login
* Friendly interface toasts
* v2! Create API endpoints for external services
* Add Markdown to the notes

## Bugs!

* Deactivate button on last click when generating hexagram so can't add to db twice when you click quickly on keyboard etc.

* Navigating direcly to a reading in the url doesn't load the data... BUT... click an anchor link there does AND there is no code on the click just the anchor link.... meh.

## General Improvements to codebase.

- Need to rename a lot of the components to simpler names... no need to be prefixed so much lah!

## Revision Log

### Major . Minor . Patch

1. MAJOR version when you make incompatible API changes,
2. MINOR version when you add functionality in a backwards-compatible manner, and
3. PATCH version when you make backwards-compatible bug fixes.

## Revisions

### v0.05

* Made inputs multiline
* Previous comments displayed
* edit and delete comments functionality. 


### v0.0.4

** Added comment box view
** posts comment to database

### v0.0.3

- Firebase calls were getting messy and providing to be too much data calling back and forth... proceed to testing the elements provided in polymerfire and see if I can get them working now quickly.... if not waiting until v1.0 release (of polymerfire!)

* Switched to using polymerfire to bind data!

* Implemented Reading View (readings/~readingID)    ugly url.... perhaps another idea?

1. Considerations for the DB. can either post comments into 
	/users/uid/readings/rid/comments
	/users/uid/readings/comments		with a reading id
	/hexagrams/hid/comments				with a reading id + userid 	(sort by hexagrams is interesting!)

	Still not sure the above is the best implementation.
	Also consider saving readings and comments in multiple locations and retrieving the list you want is then an automatic query!

### v0.0.2

** Fetches and Displays users readings (ugly + messy!)
** Question saved to DB.


### v0.0.1.

** Save readings to DB 

* Consider how the line data is stored. (to make lookup easier).... also writing to public etc. lots to consider for future versions.


### v0.0.1

- First Commit with versioning!
