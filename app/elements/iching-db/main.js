'use strict';
/**
  * Application begins here
  * 
  * All variables are to the window as an app object
  *
  */

// Initializes App.
function App() {
  this.checkSetup();

  // global current reading variable (could be part of a component)
  this.yi = new Yi();

  // Think about how to split the dom elements up
  // basically we have 3 views.... question card .... create reading .... save reading....

  // Shortcuts to DOM Elements.
  // 
  // sign in stuff
  this.userPic = document.getElementById('user-pic');
  this.userName = document.getElementById('user-name');
  this.signInButton = document.getElementById('sign-in');
  this.signOutButton = document.getElementById('sign-out');
  this.signInSnackbar = document.getElementById('must-signin-snackbar');

  // 3 main cards
  this.questionCard = document.getElementById('question-card');
  this.createReadingCard = document.getElementById('create-reading-card');
  this.displayReadingCard = document.getElementById('display-reading-card');

  // question card
  this.questionForm = document.getElementById('question-form');
  this.questionInput = document.getElementById('question-input');
  this.submitButton = document.getElementById('submit');
  
  // Readings List 
  this.readingList = document.getElementById('readings');
  
  // create reading card
  this.questionHeading = document.getElementById('question-heading');
  this.lines = document.getElementById('lines');
  this.createLineButton = document.getElementById('create-line');
  

  this.newReadingButton = document.getElementById('new-reading');
  
  // Event Listeners
  // signin
  // this.signOutButton.addEventListener('click', this.signOut.bind(this));
  // this.signInButton.addEventListener('click', this.signIn.bind(this));

  //question form
  // this.questionForm.addEventListener('submit', this.castReading.bind(this));
  // Toggle for the button.
  //var buttonTogglingHandler = this.toggleButton.bind(this);
  //this.questionInput.addEventListener('keyup', buttonTogglingHandler);
  //this.questionInput.addEventListener('change', buttonTogglingHandler);

  // create reading 
  // this.createLineButton.addEventListener('click', this.createLine.bind(this));
  
  // display reading
  // this.newReadingButton.addEventListener('click', this.newReading.bind(this));
  
  this.initFirebase();
}

// Sets up shortcuts to Firebase features and initiate firebase auth.

App.prototype.initFirebase = function() {
  // Shortcuts to Firebase SDK features.
  this.auth = firebase.auth();
  this.database = firebase.database();
  this.storage = firebase.storage();
  // Initiates Firebase auth and listen to auth state changes.
  this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
};

// Loads chat readings history and listens for upcoming ones.
App.prototype.loadReadings = function() {
  // Reference to the /readings/ database path.
  this.readingsRef = this.database.ref('readings');
  // Make sure we remove all previous listeners.
  this.readingsRef.off();

  // Loads the last 12 readings and listen for new ones.
  var setReading = function(data) {
    var val = data.val();
    app.readings = val;
    this.displayReading(data.key, val.name, val.question, val.reading, val.imageUrl);
  }.bind(this);
  this.readingsRef.limitToLast(12).on('child_added', setReading);
  this.readingsRef.limitToLast(12).on('child_changed', setReading);
};

// Saves a new reading on the Firebase DB.
App.prototype.saveReading = function( reading, q) {
  // Check that the user entered a reading and is signed in.
  console.log(reading,q);
  if (q && this.checkSignedInWithMessage()) {
    var currentUser = this.auth.currentUser;
    
    console.log(reading);
    var outcome = this.yi.iChing.lookUpReading(reading);
    
    var changingLines=[];
    for( var i=0; i<reading.length; i++) {
      if( reading[i][1] ) {
        changingLines.push('line' + i+1);
      }
    }

    var readingsRef = firebase.database().ref('readings');
    // Add a new reading entry to the Firebase Database.
    readingsRef.push({
      name: currentUser.displayName,
      question: q,
      reading: reading,
      hexagram: outcome[0].id,
      changingHex: outcome[1].id,
      changingLines: changingLines,
      photoUrl: currentUser.photoURL || '/images/profile_placeholder.png'
    }).then(function() {
    }.bind(this)).catch(function(error) {
      console.error('Error writing new reading to Firebase Database', error);
    });
  }
};

// Triggers when the auth state change for instance when the user signs-in or signs-out.
App.prototype.onAuthStateChanged = function(user) {
  if (user) { // User is signed in!
    // Get profile pic and user's name from the Firebase user object.
    var profilePicUrl = user.photoURL;
    var userName = user.displayName;

    // Set the user's profile pic and name.
    this.userPic.style.backgroundImage = 'url(' + (profilePicUrl || '/images/profile_placeholder.png') + ')';
    this.userName.textContent = userName;

    // Show user's profile and sign-out button.
    this.userName.removeAttribute('hidden');
    this.userPic.removeAttribute('hidden');
    this.signOutButton.removeAttribute('hidden');

    // Hide sign-in button.
    this.signInButton.setAttribute('hidden', 'true');

    // We load currently existing chant readings.
    this.loadReadings();
  } else { // User is signed out!
    // Hide user's profile and sign-out button.
    this.userName.setAttribute('hidden', 'true');
    this.userPic.setAttribute('hidden', 'true');
    this.signOutButton.setAttribute('hidden', 'true');

    // Show sign-in button.
    this.signInButton.removeAttribute('hidden');
  }
};

// Returns true if user is signed-in. Otherwise false and displays a message.
App.prototype.checkSignedInWithMessage = function() {
  // Return true if the user is signed in Firebase
  if (this.auth.currentUser) {
    return true;
  }

  // Display a reading to the user using a Toast.
  var data = {
    message: 'You must sign-in first',
    timeout: 2000
  };
  this.signInSnackbar.MaterialSnackbar.showSnackbar(data);
  return false;
};

// Checks that the Firebase SDK has been correctly setup and configured.
App.prototype.checkSetup = function() {
  if (!window.firebase || !(firebase.app instanceof Function) || !window.config) {
    window.alert('You have not configured and imported the Firebase SDK. ' +
        'Make sure you go through the codelab setup instructions.');
  } else if (config.storageBucket === '') {
    window.alert('Your Firebase Storage bucket has not been enabled. Sorry about that. This is ' +
        'actually a Firebase bug that occurs rarely. ' +
        'Please go and re-generate the Firebase initialisation snippet (step 4 of the codelab) ' +
        'and make sure the storageBucket attribute is not empty. ' +
        'You may also need to visit the Storage tab and paste the name of your bucket which is ' +
        'displayed there.');
  }
};


// Template for readings.
App.READING_TEMPLATE =
    '<div class="reading-container">' +
      '<div class="spacing"><div class="pic"></div></div>' +
      '<div class="reading"></div>' +
      '<div class="question"></div>' +
      '<div class="name"></div>' +
    '</div>';

App.LINE_TEMPLATE =
    '<div class="line-container">' +
      '<div class="spacing">' +
      '<div class="line"></div>' +
    '</div>';

// A loading image URL.
App.LOADING_IMAGE_URL = 'https://www.google.com/images/spin-32.gif';

App.prototype.displayReading = function(key, name, question, reading, imageUri) {
  var div = document.getElementById(key);
  // If an element for that reading does not exists yet we create it.
  if (!div) {
    var container = document.createElement('div');
    container.innerHTML = App.READING_TEMPLATE;
    div = container.firstChild;
    div.setAttribute('id', key);
    // this.readingList.appendChild(div);
  }
  div.querySelector('.name').textContent = name;
  var readingElement = div.querySelector('.reading');
  
  //readingElement.textContent = question;
  // Replace all line breaks by <br>.
  //readingElement.innerHTML = readingElement.innerHTML.replace(/\n/g, '<br>');

  // Show the card fading-in and scroll to view the new reading.
  setTimeout(function() {div.classList.add('visible')}, 1);
  this.readingList.scrollTop = this.readingList.scrollHeight;
  // this.readingInput.focus();
};


window.onload = function() {
  window.app = new App();
};







/*

<script> 
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          // User is signed in.
          app.$.toast.text = 'Hello '+ user.displayName + '! You are now logged in.';
          app.$.toast.show();
      
          // set global app's user data variables 
          app.user = user;
  
          // get users readings and setup observer for new ones
          var readingsRef = firebase.database().ref('users/' + user.uid + '/readings');
          
          readingsRef.on('value', function( snap ) {
            if (snap.val() ) app.usersReadings = _toArray( snap.val() );
            // app.usersReadings = app.usersReadings.data;
          });

          // https://firebase.google.com/docs/database/web/offline-capabilities#server-timestamps

          // since I can connect from multiple devices or browser tabs, we store each connection instance separately
          // any time that connectionsRef's value is null (i.e. has no children) I am offline
          var myConnectionsRef = firebase.database().ref('users/' + user.uid + '/connections');

          // stores the timestamp of my last disconnect (the last time I was seen online)
          var lastOnlineRef = firebase.database().ref('users/' + user.uid + '/lastOnline');

          lastOnlineRef.on('value', function(snap) {
            app.lastOnline = moment( snap.val() ).fromNow() ;

          } );
          
          var connectedRef = firebase.database().ref('.info/connected');

          connectedRef.on('value', function(snap) {
            if (snap.val() === true) {
              // We're connected (or reconnected)! Do anything here that should happen only if online (or on reconnect)

              // add this device to my connections list
              // this value could contain info about the device or a timestamp too
              var con = myConnectionsRef.push( firebase.database.ServerValue.TIMESTAMP);

              // when I disconnect, remove this device
              con.onDisconnect().remove();

              // when I disconnect, update the last time I was seen online
              lastOnlineRef.onDisconnect().set(firebase.database.ServerValue.TIMESTAMP);
            }
          });

        } else { 
          console.log('nobody')
          // No user is signed in.
          app.user={};
        }
      });
  </script>



  */